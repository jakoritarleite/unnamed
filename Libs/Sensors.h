#include <stdint.h>

class Sensors {
	public:
		uint8_t pinsOfSensorsArray[8];
		uint16_t valueOfSensors[8];
		
		typedef struct { 
			uint_fast64_t mediumIn[];
			//uint_fast32_t mediunOut;
			uint_fast64_t medium = 0;
		} rangeValues;

		typedef struct {
			uint8_t vectorIndex;
			rangeValues maximumValueOfSensor;
			rangeValues minimumValueOfSensor;
			uint_fast64_t modalValueOfSensor[];
		} Sensor;

		Sensor sensors; 

		void calibrateSensors(uint8_t[]);
		float readWhiteLine(uint16_t[]);

	private:
		void HashTool(uint_fast64_t[]);
};

uint_fast64_t Sensors::HashTool(uint_fast64_t IndexValue[]) { //IndexValue[] corresponds to modalValueOfSensor
	for (int i = 0; i < sizeof(IndexValue); i++) {
		if (sensors.modalValueOfSensor[sensors.vectorIndex] / 1000 > 0,3) {
			sensors.maximumValueOfSensor.mediumIn[sensors.vectorIndex] = sensors.modalValueOfSensor[sensors.vectorIndex];
		} else if (sensors.modalValueOfSensor[sensors.vectorIndex] / 1000 <= 0,19) {
			sensors.minimumValueOfSensor.mediumIn[sensors.vectorIndex] = sensors;modalValueOfSensor[sensors.vectorIndex];
		}
	}

	for (int i = 0; i < sizeof(sensors.maximumValueOfSensor.mediumIn); i++) {
		sensors.maximumValueOfSensor.medium += sensors.maximumValueOfSensor.mediumIn[i];
	} sensors.maximumValueOfSensor.medium /= sizeof(sensors.maximumValueOfSensor.mediumIn);

	for (int i = 0; i < sizeof(sensors.minimumValueOfSensor.mediumIn); i++) {
		sensors.minimumValueOfSensor.medium += sensors.minimumValueOfSensor.mediumIn[i];
	} sensors.minimumValueOfSensor.medium /= sizeof(sensors.minimumValueOfSensor.mediumIn);
}

void Sensors::calibrateSensors(uint8_t pinsOfSensors[]) {
	for(uint8_t i = 0; i < sizeof(pinsOfSensors); i++) {
		pinsOfSensorsArray[i] = pinsOfSensors[i];
	}

	for(sensors.vectorIndex; sensors.vectorIndex < sizeof(pinsOfSensors); sensors.vectorIndex++) {
		sensors.modalValueOfSensor[sensors.vectorIndex] = analogRead(pinsOfSensors[i]);	
	}
	
	HashTool(sensors.modalValueOfSensor);
}

float Sensors::readWhiteLine(uint16_t varToSaveTheDataSheet[]) {
	for(uint8_t i = 0; i < sizeof(varToSaveTheDataSheet); i++) {
		valueOfSensors[i] = analogRead(pinsOfSensorsArray[i]);

		if(valueOfSensors[i] > 1000) {
			valueOfSensors[i] = 1000;
		}
	}

	for(uint8_t i = 0; i < sizeof(valueOfSensors); i++) {
		varToSaveTheDataSheet[i] = valueOfSensors[i] - sensors.minimumValueOfSensor.medium;
	}

	uint16_t positionOfTheLineAccordingTheSensorsValues = 0;
	uint16_t sumOfPosition = 0;

	for(uint8_t i = 0; i < sizeof(varToSaveTheDataSheet); i++) {
		positionOfTheLineAccordingTheSensorsValues += varToSaveTheDataSheet[i] * (i * 1000);
		sumOfPosition += varToSaveTheDataSheet[i];
	}

	return( positionOfTheLineAccordingTheSensorsValues / sumOfPosition );
}