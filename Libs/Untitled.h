#include <stdint.h>

typedef struct {
	bool keyOne;
	bool keyTwo;
} motorKey;

motorKey motorA;
motorKey motorB;

void setMotorsKey(short int directionA, short int directionB) {
	//1 goes forward
	//0 goes back
	if (directionA == 1) {
		motorA.keyOne = HIGH;
		motorA.keyTwo = LOW;
	} else if (directionA == 0) {
		motorA.keyOne = LOW;
		motorA.keyTwo = HIGH;
	} 

	if (directionB == 1) {
		motorB.keyOne = HIGH;
		motorB.keyTwo = LOW;
	} else if (directionB == 0) {
		motorB.keyOne = LOW;
		motorB.keyTwo = HIGH;
	}

	if (directionA == 2 && directionB == 2) {
		motorA.keyOne = HIGH;
		motorA.keyTwo = HIGH;
		motorB.keyOne = HIGH;
		motorB.keyOne = HIGH;
	}
}

void motorsToWork(int speedA, int speedB) {
	digitalWrite(motorAs1, motorA.keyOne);
	digitalWrite(motorAs2, motorA.keyTwo);
	digitalWrite(motorBs1, motorB.keyOne);
	digitalWrite(motorBs2, motorB.keyTwo);

	analogWrite(pwmA, speedA);
	analogWrite(pwmB, speedB);
}

void encoderToWork() {
	long int newPositionA = myEncA.read();
	long int newPositionB = myEncB.read();

	if (useBluetooth) {
		//Code to use bluetooth
	}

	if ((newPositionA - firstPositionA) >= encoderLastPosition && (newPositionB - firstPositionB) >= encoderLastPosition) {
		while (true) {
			setMotorsKey(2, 2);
			motorsToWork(0, 0);
		}
	}
}